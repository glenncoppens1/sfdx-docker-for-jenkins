# SFDX Docker image
This image should be used as a base image in a Jenkins environment to run as a slave.

# Jenkins Docker Slave
* https://devopscube.com/docker-containers-as-build-slaves-jenkins/