FROM jenkinsci/slave

LABEL maintainer="glenn.coppens@gmail.com"

USER root

# update and install tools
RUN apt-get -y update
RUN apt-get -y install git-core curl build-essential openssl libssl-dev
RUN curl -sL https://deb.nodesource.com/setup_9.x | bash -
RUN apt-get -y install nodejs
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*

# confirm installation
RUN nodejs -v && \
    npm -v

# install sfdx 
RUN npm install --global --quite sfdx-cli
RUN sfdx update

USER jenkins